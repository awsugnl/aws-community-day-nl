---
title: "How to use modern serverless with GraphQL"
date: 2024-10-03T14:10:00
speakers:
    - "igor-soroka"
room: "7"
agenda: "1410"
duration: "30"
---

In the year 2024, serverless is much more than just AWS Lambda Functions. Now, it is a full-blown ecosystem of managed services with the concept of paying for what you use. The API level could be written without computing services like Lambda Functions or containers with Fargate. I will show the audience how to deploy the GraphQL API with JavaScript Resolvers, EventBridge, and CDK. Disclaimer: no Lambda Functions will be called during the talk and a demo.