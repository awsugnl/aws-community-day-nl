---
title: "Unlocking Innovation with Red Hat OpenShift Service on AWS: A Collaborative Path to Success"
date: 2024-10-03T13:00:00
speakers:
    - "anton-nesterov"
room: "9"
agenda: "1300"
duration: "60"
sponsors:
    - red-hat
---

Be aware this session is 60 minutes.

In today's fast-paced cloud-native world, businesses need a seamless platform to build, deploy, and manage applications with speed and scalability. Join us for an engaging and interactive session where we introduce Red Hat OpenShift Service on AWS (ROSA), a powerful collaboration between Red Hat and AWS.
Through a lively discussion and a fun role-play with different "hats" representing key stakeholders-CTO, architect, and developer-we'll demonstrate how ROSA empowers organizations to streamline operations, improve collaboration, and accelerate innovation. Whether you are new to OpenShift, AWS, or both, this session will provide valuable insights into the synergy of these platforms. We'll cover use cases, highlight the value of containerized applications on a managed Kubernetes service, and show a live demo of ROSA in action.
Expect to walk away with an understanding of how ROSA simplifies hybrid cloud deployments and delivers flexibility and scalability, all while keeping DevOps teams productive and happy. Don't miss the chance to see how this partnership can drive innovation for your business!
