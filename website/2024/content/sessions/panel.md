---
title: "Closing Keynote: Innovation and Responsibility in the AWS Cloud Era"
date: 2024-10-03T16:45:00
room: "11"
duration: 45
agenda: "1630"
speakers:
    - angelina-best
    - carrie-carrasco
    - lieke-hamers
    - juliette-van-der-laarse
    - frauke-wessel
---

In this engaging fireside chat we will explore the balance between innovation and responsibility in the AWS cloud environment. We will discuss the lessons that can be derived from the CrowdStrike incident regarding secure innovation, the evolving nature of cloud leadership amid shared responsibility, and the role of open source in driving innovation while fostering accountability. We will also highlight the power of community-driven cloud development and discuss the industry's responsibility to promote diversity and inclusive innovation. The session will encourage interactive audience participation to address these pressing themes.