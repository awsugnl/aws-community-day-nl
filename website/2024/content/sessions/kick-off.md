---
title: "AWS Community Day NL 2024"
date: 2024-10-03T09:15:00
speakers:
    - "danielle-gorlick"
    - "kieren-wade"
    - "tim-nolet"
    - "milou-verheij"
    - "martijn-van-dongen"
room: "11"
duration: 75
agenda: "0915"
---

We kick-off this AWS Community Day with fireside chats with Milou Verheij, Danielle Gorlick, Keren Wade and Tim Nolet. They'll share some valuable insights, technical tips & tricks, and lessons learned from their journeys. 