---
title: "Yaika Zeeman"
headline: "AWS Cloud Engineer"
linkedin: "https://www.linkedin.com/in/yaika-zeeman/"
---
Yaika Zeeman is an AWS Cloud Engineer at Sogeti with a background in IT support, leveraging this experience to efficiently manage cloud infrastructure for various clients. She is actively involved in sustainability projects within the Sogeti AWS community, focusing on optimizing resources and reducing environmental impact.
