---
title: "Frauke Wessel"
headline: "Supervisory Board Member"
linkedin: "https://www.linkedin.com/in/frauke-wessel-01a0b16/"
speaker: "closing"
---

Experienced CIO and business executive worked in airline industry, healthcare, telco and energy. Supervisory board member, coach, mentor and trainer for executive board members. Frauke helps organizations with Change: determine new strategy, align organization to new goals, execute transformation; She has the innate ability to guide organisations towards the future. Her approach is holistic, people, proces and technology need to change in sync to keep the soul of an organization alive.
