---
title: "Igor Soroka"
headline: "Indie Serverless Consultant"
linkedin: "https://www.linkedin.com/in/igor-soroka/"
---

I am a software consultant working with AWS and serverless for the last couple of years. My background includes experience with companies of all sizes. I value collaboration with a focus on innovations in the software development process.
