---
title: "Stef van der Wel"
headline: "Software Engineer | Cloud & Serverless Enthusiast | Passionate about Green IT and FinOps"
linkedin: "https://www.linkedin.com/in/stef-van-der-wel/"
---

Stef works as Software Engineer at de Volksbank where he and his team pioneer Serverless technologies. Stef's mission is to inspire and enable other teams by sharing his knowledge and experience about topics such as Cloud, Serverless, Green IT and FinOps.