---
title: "Sam Williams"
headline: "The Serverless Obsessive"
linkedin: "https://www.linkedin.com/in/sam-complete-coding/"
---

"Sam is a Serverless Obsessive who runs Complete Coding, helping developers learn Serverless and companies make the most of the competitive advantage that Serverless gives them.

Previous projects include:
- Designing a chat platform that currently resolves over 250,000 customer inquiries a month for international retailers and local government
- Architecting a backend system to support a 3D clothing visualisation tool
- Building a solution to manage millions of dollars of communication software
- Launching a profitable Serverless Startup in under 30 days

He has also been teaching cloud-based software development for 6 years and has taught Serverless development to thousands of people.

The Complete Coding Youtube channel now has over 16,000 subscribers and 1 million views"