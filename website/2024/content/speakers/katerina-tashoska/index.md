---
title: "Katerina Tashoska"
headline: "AWS Architect | Consultant @Xebia"
linkedin: "https://www.linkedin.com/in/katerinapackova/"
---

Software development passion led me to the dynamic realm of cloud computing, where I delved deep into AWS cloud solutions. This exploration has been incredibly rewarding, enabling me to craft innovative solutions that significantly elevate client businesses. Today, my focus is on cloud modernization, where I guide teams in fully embracing and thriving on their cloud journey, unveiling new opportunities and pushing the boundaries of what's possible.