---
title: "Guilherme Dalla Rosa"
headline: "CTO at MerCloud"
linkedin: ""
---

Guilherme Dalla Rosa is a seasoned software engineer with extensive experience in the industry. He has worked on numerous projects across various industries in Brazil, Ireland, and the UK. Currently, he serves as the CTO of MerCloud, a B2B e-commerce platform that simplifies the sales process for companies. With his years of experience and expertise, Guilherme is passionate about helping businesses leverage technology to improve their processes and achieve their goals.