---
title: "Carrie Carrasco"
headline: "Director Hybrid Platform Specialists"
linkedin: "https://www.linkedin.com/in/carriecarrasco/"
speaker: "closing"
---

Fulfills a crucial role as an Open leader within the organization in the field of digital transformation and technology. She serves as a role model for openness, inclusion and diversity, especially for Platform Specialists within Red Hat in the Netherlands and Central Europe.

She leads the teams of platform specialists and specialist architects with whom she develops and delivers open innovation for her customers. After graduating in Theoretical Physics at Salamanca University, a Master of Science (MSc) degree in Information Technology at Portsmouth University (UK), Carrie started her career at Salamanca University while working on a PhD on Applied Physics, developing the first Digital Labs.

https://www.linkedin.com/in/carriecarrasco
