---
title: "Angelina Best"
headline: "CEO Levi9 & Advisory Board Member NLdigital "
linkedin: "https://www.linkedin.com/in/angelina-best-driving-digital-transformation/"
speaker: "closing"
---

Angelina Best is the CEO of Levi9 Technology Services. She has over 20 years of experience at AWS and Microsoft, where she was responsible for building and leading teams that support startups, SMEs, and large enterprise customers and partners.

Angelina is a keynote and press speaker, an advocate for inclusion, diversity, and equity, and a member of the NLdigital Advisory Board. She is passionate about building teams, driving innovation, and delivering customer success.