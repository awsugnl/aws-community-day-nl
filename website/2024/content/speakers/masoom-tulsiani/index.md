---
title: "Masoom Tulsiani"
headline: "Cloud Architect at Rackspace Technology"
linkedin: "https://www.linkedin.com/in/masoom/"
---

Cloud Architect at Rackspace, AWS Community Speaker, Spoke at AWS Community Day Nordics, Passionate about Blogging on AWS Serverless, Kubernetes & Building AWS Landing Zones. 

Sensonize Speaker Profile: https://sessionize.com/masoom-tulsiani/
Twitter Profile: @Cloudjedi_. https://x.com/cloudjedi_
