---
title: "Carlo van Overbeek"
headline: "Security DevOps Engineer Cloud"
linkedin: "https://www.linkedin.com/in/carlo-van-overbeek-4b49b61b/"
---

A graduate PhD in physical chemistry turned DevOps with a particular interest in infrastructure and security automation. Most skilled in Terraform (preferably in AWS) and Python. Sometimes dipping a toe in JavaScript and Bash when unavoidable. Currently working on automating security for Geldmaat in AWS at Schuberg Philis. Privately working on gamification of encryption and of trusted knowledge.