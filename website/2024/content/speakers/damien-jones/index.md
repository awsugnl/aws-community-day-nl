---
title: "Damien Jones"
headline: "💻 UK AWS Consultant 🎤 Speaker 🦈 Creator @ amazonwebshark ☁️ AWS Certified | Fin Fan | Dog Dad | He/Him"
linkedin: "https://www.linkedin.com/in/mrdamienjones"
---

Damien Jones is an experienced data professional and cloud enthusiast with a proven history of designing, building, and optimizing data solutions to drive business insights. He is proficient in SQL, cloud architectures and data modelling methodologies, with a passion for using emerging technologies to solve complex business challenges and optimize data-driven decision-making processes. Damien is active in the cloud community as a content creator and public speaker.

Outside of work, he is a keen runner, amateur gardener, dance music enthusiast and dog dad to two furry boys.

https://amazonwebshark.com/about/