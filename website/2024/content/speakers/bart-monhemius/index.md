---
title: "Bart Monhemius"
headline: "Solutions Architect / Tech Lead / Green IT Advocate at de Volksbank"
linkedin: "https://www.linkedin.com/in/bmonhemius/"
---

Bart Monhemius has been leveraging AWS to solve complex problems since the early days of his career. Having worked in both startup and corporate environments, he uses his experiences to make the best of both worlds. He applies his passion and empathy to inspire others. He believes that people are most effective when given freedom, trust, and responsibility.

Today, Bart is a Solutions Architect and Green IT Advocate at de Volksbank. He is passionate about social and environmental responsibility and is dedicated to making technology more sustainable.