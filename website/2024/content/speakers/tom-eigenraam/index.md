---
title: "Tom Eigenraam"
headline: "AWS Cloud Consultant at CloudNation"
linkedin: "https://wwww.linkedin.com/in/tom-eigenraam-1ba01780"
---
Tom Eigenraam started his career in cloud in 2019 and has a background in strategic product design. After working as AWS DevOps engineer and assessing many different AWS environments through Well-Architected Framework Reviews,
he developed a specific interest in helping customers and colleagues with security & compliance, FinOps and other optimization-related challenges. He also is involved in pre-sales trajectories and helping customers with kickstarting 
their migration journeys through several AWS funding programs, creating business cases and securing funding.
