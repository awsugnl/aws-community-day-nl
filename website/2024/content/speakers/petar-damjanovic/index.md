---
title: "Petar Damjanovic"
headline: "Software Architect at Levi9"
linkedin: "https://www.linkedin.com/in/petar-damjanović-2b986469/"
---

Petar is a software architect who began his career as a scholarship student for Levi9 in 2013. He has advanced to his current position through many big data-related projects in several business domains. Inspired by participating early on in solving a challenge of taming billions of raw events per day, decided to focus on the data engineering learning path which lasts to this very day. He considers himself systematic, detail-oriented, cautious and hard-working, with a strong fondness to the AWS ecosystem. Loves to understand business challenges and tries to balance helicopter view and small details, enabling projects to succeed.
