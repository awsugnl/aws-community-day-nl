---
title: "Jochem Kleine"
headline: "Cloud Consultant by day, Indie Developer by night"
linkedin: "https://www.linkedin.com/in/jochem-kleine/"
---

I'm a jack of many trades and a master of some. 

By day, I work as a Cloud Consultant at Xebia, helping clients kickstart their cloud journey, and bringing their existing AWS cloud platforms and applications to the next level. 
In my free time, I go back to where it all started for me: app development. 

Since 2015, I've been developing apps on and off in different areas (social, education, gaming) and for different platforms. No smash hit just yet, but it's ought to happen one of these days right? 

The app I released most recently is Shimmers: a cosy indie game where you rekindle with Shimmers of old. 