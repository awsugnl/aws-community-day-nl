---
title: "Steffan Norberhuis"
headline: "Founder Rocketleap"
linkedin: "https://www.linkedin.com/in/steffannorberhuis/"
---

Steffan Norberhuis is the founder of Rocketleap, a company that helps organizations launch their next step in their AWS cloud journey. Previously, he worked as a freelance AWS & DevOps Consultant. He loves helping teams create synergy with the business side to deliver value quickly through the power of AWS.