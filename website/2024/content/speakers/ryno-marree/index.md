---
title: "Ryno Marree"
headline: "Senior Solution Software Engineer @ PostNL"
linkedin: "https://www.linkedin.com/in/ryno-marree/"
---

A Cloud Engineer with a background and passion for data and enterpreneurship and over five years of experience building AWS Serverless solutions. The fun does not stop at work, at home there is a lot of tinkering with servers and self-hosting FOSS software.
