---
title: "Vadym Kazulkin"
headline: "Head of Development at ip.labs in Bonn, Germany"
linkedin: "https://www.linkedin.com/in/tnolet/"

---

Vadym Kazulkin is Head of Development at ip.labs GmbH, a 100% subsidiary of the FUJIFLM Group, based in Bonn. ip.labs is the world's leading white label e-commerce software imaging company. Vadym has been involved with the Java ecosystem for over twenty years. His focus and interests currently include the design and implementation of highly scalable and available applications, Serverless and AWS Cloud. Vadym is the co-organizer of the Java User Group Bonn meetup and AWS Community Builder in the Serverless category and a frequent speaker at various Meetups and conferences.