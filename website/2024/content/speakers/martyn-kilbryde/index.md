---
title: "Martyn Kilbryde"
headline: "Head of Engineering at Chemist4U"
linkedin: "https://www.linkedin.com/in/martyn-kilbryde/"
---

Martyn Kilbryde is an experienced software architect and engineer with many years of experience working with global highly transactional web applications, specialising in building and delivering large complex systems from scratch. He is especially interested in event-driven and serverless architectures.