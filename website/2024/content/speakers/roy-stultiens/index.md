---
title: "Roy Stultiens"
headline: "Senior Security Specialist"
linkedin: "https://www.linkedin.com/in/roystultiens"
---
Roy Stultiens is a Senior Security Specialist at Secura. With a focus on Cloud Security, he is one of Secura's Cloud experts and involved in many cloud penetration tests. He has seen numerous environments and successfully exploited many of them, ranging from data leaks to privilege escalations and account takeovers. Apart from his technical work, Roy is involved in cloud and containerization security trainings by developing lab environments and organizing workshop.