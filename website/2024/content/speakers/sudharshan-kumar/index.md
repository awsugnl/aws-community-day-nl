---
title: "Sudharshan Kumar"
headline: "Senior Security Specialist"
linkedin: "https://www.linkedin.com/in/sudharshankr94"
---
Sudharshan Kumar is a Senior Security Specialist at Secura focusing on Cloud, web applications and active directory security. Being a part of Secura's cloud experts group, he has performed several pentests on AWS and Azure corporate environments wherein he has found multiple privilege escalation scenarios. He firmly believes in the motto: "For me personally, cloud security isn't a worry. My data is such a mess that no one would find anything anyway", or not.