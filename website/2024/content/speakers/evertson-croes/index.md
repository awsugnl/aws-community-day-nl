---
title: "Evertson Croes"
headline: "Organising success via Luminis"
linkedin: "https://www.linkedin.com/in/evertson-croes/"
---

Evertson has been working at Luminis for almost 10 years in different capacities. He has worked as an engineer, architect, scrum master, and product owner. Evertson focuses on finding the missing pieces in the projects he works on and fills them in. He takes responsibility for providing for providing structure and clarity, enabling the team(s) he works with to excel. Oh, and he also likes AWS.