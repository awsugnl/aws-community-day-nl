---
title: "Pubudu Jayawardana"
headline: "AWS Community Builder | Senior Solutions Software Engineer @ PostNL"
linkedin: "https://www.linkedin.com/in/pubudusj/"
---

 An AWS Community Builder and a serverless enthusiast with more than a decade of commercial experience as a software developer and devops engineer.
 Love to build stuff with AWS Serverless and blog/share them regularly.