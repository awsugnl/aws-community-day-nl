---
title: "Michal Salanci"
headline: "Senior DevOps Engineer in Deutsche Telekom Systems Solutions, Slovakia"
linkedin: "https://www.linkedin.com/in/michal-salanci-🫐-3496724b/"
---
My name is Michal Salanci, and I am a Senior DevOps Engineer at Deutsche Telekom in Slovakia. 

For many years, I worked as a Network Engineer, handling various networking equipment such as routers, switches, firewalls, load balancers, and more. In 2021, I made a career shift towards AWS, where I primarily focus on networking, security, serverless and containers. 
I am AWS Community Builder in category Containers and also Leader of AWS User Group Kosice Slovakia and Reykjavik Iceland.

https://michal-salanci.s3.eu-central-1.amazonaws.com/index.html
