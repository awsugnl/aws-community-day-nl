---
title: "Anton Nesterov"
headline: "Senior EMEA Global Black Belt for Red Hat Cloud Services"
linkedin: "https://www.linkedin.com/in/anton-nesterov-68439828/"
---

Anton is a seasoned platform engineer with over a decade of experience in cloud technologies and DevOps practices. Passionate about optimizing tech stacks and driving innovation, Anton has successfully led multiple migrations to managed services, significantly improving operational efficiency and business outcomes. Currently at Red Hat, Anton is focused on advocating for strategic adoption of managed cloud services to help organizations unlock their full potential. Besides field work, you could spot him speaking at events and running public workshops.