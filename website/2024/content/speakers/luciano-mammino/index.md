---
title: "Luciano Mammino"
headline: "AWS Serverless Hero, Senior Architect @ fourTheorem"
linkedin: "https://www.linkedin.com/in/lucianomammino/"
---

Luciano is an AWS Serverless Hero, author and speaker. In his day-to-day job at
fourTheorem he helps companies to get the best out of the Cloud and AWS. He is
an active speaker and in the last few years, he has delivered more than 130
talks in conferences and meetups around the World. He is also the author of the
famous book [Node.js Design Patterns](https://nodejsdesignpatterns.com) and he
is currently working on
[Crafting Lambda Functions in Rust](https://rust-lambda.com/).
