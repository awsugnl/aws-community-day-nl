---
title: "Christiaan Rakowski"
headline: "Solutions Architect at XXImo Holding"
linkedin: "https://www.linkedin.com/in/christiaanrakowski/"
---

Christiaan Rakowski is an avid learner with over a decade of experience in software development on the Microsoft stack in an on-prem environment.
Over the past year, he has been rapidly expanding his knowledge of AWS services to ensure XXImo's migration to the cloud is a success.