---
title: "Lieke Hamers"
headline: "Field CTO Dell Technologies Nederland"
linkedin: "https://www.linkedin.com/in/liekehamers/"
speaker: "closing"
---

Lieke Hamers is the Field Chief Technology Officer (CTO) and the Connected Sustainable Society Lead at Dell Technologies. She has more than two decades of experience in the IT industry, with a focus on consulting, strategy and business development in both software and hardware. She is passionate about making the Netherlands a better place and exploring the intersection of technology and society.

As the Field CTO, she leads industry digital transformation engagements and opportunities with Dell Technologies' customers, leveraging her deep industry knowledge and experience. She acts as a technical and thought leader, bridging the gap between the company's technology vision and the needs of customers in the Netherlands. She is responsible for promoting Dell Technologies' solutions and capabilities in the areas of cloud, data, artificial intelligence, and edge computing. She also empowers and mentors women in tech, advocates for STEM education, and builds strong partnerships and ecosystems.