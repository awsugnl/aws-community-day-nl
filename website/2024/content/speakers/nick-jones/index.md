---
title: "Nick Jones"
headline: "Global Head of Research @ WithSecure Consulting"
linkedin: "https://www.linkedin.com/in/nickojones/"
---

Nick is the Global Head of Research at WithSecure Consulting, where he focuses on AWS security and attack detection in advanced, cloud-native organisations. He has been delivering offensive security testing, consultancy and support to a world-wide client base (including some of the world's largest financial organisations) for a decade, and led WithSecure Consulting's cloud security team for the last 5 years. Nick has previously spoken at a number of conferences and events including fwd:cloudsec, DEF CON Cloud Village, Disobey, T2, and several AWS User Groups and community days. Nick is also an AWS Community Builder.