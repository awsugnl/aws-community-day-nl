---
title: "Moukarram Kabbash"
headline: "Leader Software Development at SenseGuard GmbH"
linkedin: "https://www.linkedin.com/in/mkabbash/"
---

Moukarram is Leader Software Development at SURU, where he leads the development of technology solutions to promote water safety and prevent water waste. He specializes in cloud computing, serverless technology, and microservices, and is dedicated to helping developers deliver maximum value through effective software development. He is passionate about sharing ideas and discussing the latest advancements in the field.