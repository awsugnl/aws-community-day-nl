---
title: "Juliette van der Laarse"
headline: "PO Engineering Ecosystems at NN Group "
linkedin: "https://www.linkedin.com/in/juliettevanderlaarse/"
speaker: "closing"
---

Juliette is an IT strategist and innovator, currently the Product Owner of Engineering Ecosystems at NN Group. She is the founder and chairwoman of NN Women in IT, where she actively promotes gender equality in tech. Before joining NN, Juliette led a startup specialized in the development of simulations for training and education, which eventually led to the creation of an AI-driven platform for personalized learning, earning her international recognition. Her diverse background includes research on North Korea and AI projects for social impact. In 2018, she was named one of the 400 most inspirational Dutch women and featured as a technology role model by the European Parliament.