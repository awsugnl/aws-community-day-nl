---
title: "Erik van Dijk"
headline: "Tech Lead AWS / DevOps engineer at CloudNation"
linkedin: "https://www.linkedin.com/in/erik-van-dijk-96316498/"
---

Erik van Dijk is a DevOps engineer with over 10 years of experience. He mainly worked for companies in the banking and insurance sector. He Helps them creating landing zones and/or platforms with automation and infrastructure as code so they can start start there cloud journey in a secure matter ad-hearing to rules and regulations. 